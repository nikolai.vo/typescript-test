import { FormControl } from "./ts/formControl";
import { FormGroup } from "./ts/formGroup";
import { RequireValidator } from "./ts/validator/requaerValidator";
import { EmailValidator } from "./ts/validator/emailValidator";
import { PatternValidator } from "./ts/validator/patternValidator";
import { MaxValueValidator } from "./ts/validator/maxValueValidator";
import { MinValueValidator } from "./ts/validator/minValueValidator";
import { NumberValidator } from "./ts/validator/numberValidator";
import { MinLengthValidator } from "./ts/validator/minLengthValidator";
import { MaxLengthValidator } from "./ts/validator/maxLengthValidator";

const formGroup = new FormGroup('test-form', [
  new FormControl('input-1', null, [new RequireValidator('')]),
  new FormControl('input-2', null, [new EmailValidator('')]),
  new FormControl('input-3', null, [new NumberValidator('')]),
  new FormControl('input-4', null, [new MinLengthValidator(4)]),
  new FormControl('input-5', null, [new MaxLengthValidator(10)]),
  new FormControl('input-6', null, [new MaxValueValidator(5)]),
  new FormControl('input-7', null, [new MinValueValidator(10)]),
  new FormControl('input-8', null, [new PatternValidator(/[0-9]{4} *[0-9]{4} *[0-9]{4} *[0-9]{4}/)])

]);

// console.log(formGroup.value);

formGroup.valueChange.subscribe((change: string) => {
  console.log('FormGroup change', change);
});
