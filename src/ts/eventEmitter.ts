
//«транслятор» или «эмиттер» событий
export class EventEmitter {
  events: Array<any>;
  constructor() {
    this.events = [];
  }
  // emit генерирует события, которые мы хотим транслировать
  emit(...eventData: Array<any>): void {
    if (this.events) {
      this.events.forEach(eventSubscr => eventSubscr(...eventData));
    }
  }
  //subscribe принимает  события и обрабатывает
  subscribe(...eventSubscr:Array<any>) {
    if (!this.events) {
      this.events = [];
    }

    this.events.push(...eventSubscr);
  }

}
