import { EventEmitter } from './eventEmitter';
import { Validator } from './validator/validator';


export class FormControl {
  id: string;
  initialValue: string | number | null;
  validators: [Validator];
  status: boolean;
  valueChange: EventEmitter;
  statusChange: EventEmitter;
  input: HTMLInputElement;
  value: string;
  constructor(id: string, initialValue: string | number | null, validators: [Validator]) {
    this.id = id;
    this.initialValue = initialValue;
    this.validators = validators;
    this.status = false;
    this.valueChange = new EventEmitter();
    this.statusChange = new EventEmitter();
    this.input = document.getElementById(id) as HTMLInputElement;
    this.value = this.input.value;

    // регистрируем обработчик события change для формы
    this.input.addEventListener('change', (e: Event) => {

      //every возвращает true, если вызов callback вернёт true для каждого элемента
      this.status = this.validators.every(function (valid: Validator) {
        return valid.validate((e.target as HTMLInputElement).value );
      });

      this.value = this.input.value;

      //генерируем события
      this.valueChange.emit({
        inputIdValue: this.value,
        statusValue: this.status

      });

      this.statusChange.emit({
      	inputIdValue: this.value,
      	statusArr: this.validators
      });

      if (this.status === false) {
        this.input.style.background = 'red';
      } else {
        this.input.style.background = 'green';
      }
    });
  }
}
