import { EventEmitter } from './eventEmitter';
import { FormControl } from './formControl';

export class FormGroup {
  id: string;
  controls: FormControl[];
  status: boolean[] | boolean;
  value: object;
  valueChange: EventEmitter;
  statusChange: EventEmitter;

  constructor(id: string, controls: FormControl[]) {
    this.id = id;
    this.controls = controls;
    this.status = false;
    this.value = {};
    this.valueChange = new EventEmitter();
    this.statusChange = new EventEmitter();

    // перебираем массив контроллеров
    this.controls.forEach((element: FormControl) => {

      //принимаем и обрабатываем события
      element.valueChange.subscribe((changeElem: EventEmitter) => {

        //получаем статус каждого контролера
        this.status = this.controls.map(function (elem: FormControl) {
          return elem.status;
        });
        // проверяем все статусы на тру
        this.status = this.status.every(function (valid: boolean): boolean {
          return valid === true;
        });

        this.value = this.controls.map(function (elem: FormControl) {
          return elem.value;
        });

        this.valueChange.emit({
          inputIdValue: this.value,
          statusValue: this.status

        });
      });
    });
  }
}
