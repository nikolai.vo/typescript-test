
import { Validator } from './validator';

//проверка на пустую строку
export class RequireValidator extends Validator {
  constructor(value: string) {
    super(value);
    this.value = value;
  }
  validate(value: string): boolean {
    if (value === '') {
      return false;
    } else {
      return super.validate(value);
    }
  }
}
