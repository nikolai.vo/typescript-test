import { Validator } from './validator.js';

//валидация на минимальное число
export class MinValueValidator extends Validator {
  minValue: number;
  constructor(minValue: number) {
    super('');

    this.minValue = minValue;

  }
  validate(value: number): boolean {
    return value > this.minValue
  }
}
