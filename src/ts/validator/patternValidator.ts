import { Validator } from './validator.js';


//валидация на регулярное выражение
export class PatternValidator extends Validator {
  pattern: RegExp;
  constructor(pattern: RegExp) {
    super('');

    if (pattern == null) {
      throw new Error('pattern is not define!!!');
    }
    this.pattern = pattern;

  }
  validate(value: string) {
    // return value.match(this.pattern);
    return this.pattern.test(value);
  }
}
