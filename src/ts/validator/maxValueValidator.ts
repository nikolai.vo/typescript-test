import { Validator } from './validator.js';

//валидация на максимальное число
export class MaxValueValidator extends Validator {
  maxValue: number;
  constructor(maxValue: number) {
    super('');

    this.maxValue = maxValue;

  }
  validate(value: number): boolean {
    return value < this.maxValue;
  }
}
