import { Validator } from './validator.js';

//валидация на минимальное количество символов
export class MinLengthValidator extends Validator {
  minLength: number;
  constructor(minLength: number) {
    super('');

    this.minLength = minLength;

  }
  validate(value: string): boolean {
    return value.length > this.minLength
  }
}
