import { Validator } from './validator.js';

// валидация на мфксимальное количество символов
export class MaxLengthValidator extends Validator {
  maxLength: number;
  constructor(maxLength: number) {
    super('');

    this.maxLength = maxLength;

  }
  validate(value: string): boolean {
    return value.length < this.maxLength
  }
}
