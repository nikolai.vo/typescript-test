import { Validator } from './validator.js';


//валидация на число
export class NumberValidator extends Validator {
  value: string;
  regExp: RegExp;
  constructor(value: string) {
    super(value);
    this.value = value;
    this.regExp = /\-?\d+(\.\d{0,})?/; //целые числа и числа с плавающей точкой
  }

  validate(value: string): boolean {
    return this.regExp.test(value);
  }
}
